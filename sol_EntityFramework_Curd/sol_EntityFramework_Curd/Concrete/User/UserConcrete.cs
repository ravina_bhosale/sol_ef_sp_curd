﻿using sol_EntityFramework_Curd.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sol_EntityFramework_Curd.User.Interface;
using sol_EntityFramework_Curd.EF;
using System.Data.Entity.Core.Objects;

namespace sol_EntityFramework_Curd.Concrete
{
    public class UserConcrete : IUserConcrete
    {
        #region Declaration
        private UserModelEntities dbObj = null;
        #endregion

        #region Constructor
        public UserConcrete()
        {
            dbObj = new UserModelEntities();
        }
        #endregion

        #region public method

        public async Task<dynamic> SetData(string command, IUserEntity entityObj, Action<int?, string> storedProcedureOutPara = null)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;

            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dbObj
                    .uspSetUser
                    (
                        command,
                        entityObj?.UserId,
                        entityObj?.FirstName,
                        entityObj?.LastName,
                        entityObj?.UserLogin?.UserName,
                        entityObj?.UserLogin?.Password,
                        entityObj?.UserCommunication?.MobileNo,
                        entityObj?.UserCommunication?.EmailId,
                        status = new ObjectParameter("Status", typeof(int)),
                         message = new ObjectParameter("Message", typeof(string))
                    );
                    storedProcedureOutPara(Convert.ToInt32(status.Value), message.Value.ToString());

                    return getQuery;

                });
            }

            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}
