﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.CommonRepository
{
    public interface IInsert<TEntity> where TEntity : class 
    {
        Task<Boolean> InsertAsync(TEntity entityObj);
    }
}
