﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.CommonRepository
{
    public interface ISetUser<TEntity> where TEntity : class
    {
        Task<dynamic> SetData(String Command, TEntity entityObj, Action<int?, String> storedProcedureOutPara=null);
    }
}
