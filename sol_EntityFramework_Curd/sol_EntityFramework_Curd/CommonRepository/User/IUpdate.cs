﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.CommonRepository
{
    public interface IUpdate<TEntity> where TEntity : class
    {
        Task<Boolean> UpdateAsync(TEntity entityObj);
    }
}
