﻿using sol_EntityFramework_Curd.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sol_EntityFramework_Curd.User.Interface;
using sol_EntityFramework_Curd.Concrete;

namespace sol_EntityFramework_Curd.Repository.User
{
    public class UserRepository : IUserRepository
    {
        #region Declaration
        private UserConcrete userConcreteObj = null;
        #endregion

        #region Constructor
        public UserRepository()
        {
            userConcreteObj = new UserConcrete();
        }
        #endregion

        #region public method

        public async Task<bool> InsertAsync(IUserEntity entityObj)
        {
            int? Status = null;
            String Message = null;

            try
            {
                await userConcreteObj.SetData
                    (
                        "Insert",
                        entityObj,
                        (leStatus, leMessage) =>
                        {
                            Status = leStatus;
                            Message = leMessage;

                        });
                           return (Status == 1) ? true : false;
            }

            catch(Exception)
            {
                throw;
            }
        }

        public async Task<bool> UpdateAsync(IUserEntity entityObj)
        {
            int? Status = null;
            String Message = null;

            try
            {
                await userConcreteObj.SetData
                    (
                        "Update",
                        entityObj,
                        (leStatus, leMessage) =>
                        {
                            Status = leStatus;
                            Message = leMessage;

                        });
                return (Status == 1) ? true : false;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteAsync(IUserEntity entityObj)
        {
            int? Status = null;
            String Message = null;

            try
            {
                await userConcreteObj.SetData
                    (
                        "Delete",
                        entityObj,
                        (leStatus, leMessage) =>
                        {
                            Status = leStatus;
                            Message = leMessage;

                        });
                return (Status == 1) ? true : false;
            }

            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
