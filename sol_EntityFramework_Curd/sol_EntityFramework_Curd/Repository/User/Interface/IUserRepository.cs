﻿
using sol_EntityFramework_Curd.CommonRepository;
using sol_EntityFramework_Curd.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.Repository.Interface
{
    public interface IUserRepository : IInsert<IUserEntity>,IUpdate<IUserEntity>,IDelete<IUserEntity>
    {
    }
}
