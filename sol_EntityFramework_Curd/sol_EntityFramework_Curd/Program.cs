﻿using sol_EntityFramework_Curd.Repository.User;
using sol_EntityFramework_Curd.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                UserRepository userObj = new UserRepository();

                //#region Insert

                //Boolean flag = await userObj.InsertAsync(new UserEntity()
                //{
                //    FirstName = "Geeta",
                //    LastName = "Rane",

                //    UserLogin = new UserLoginEntity()
                //    {
                //        UserName = "geeta111",
                //        Password = "12321",
                //    },

                //    UserCommunication = new UserCommunicationEntity()
                //    {
                //        MobileNo = "9854321679",
                //        EmailId = "geeta@yahoo.com"
                //    }
                //});
                //#endregion

                //#region Update

                //Boolean flag = await userObj.UpdateAsync(new UserEntity()
                //{
                //    UserId=3,
                //    FirstName = "Komal",
                //    LastName = "Shinde",

                //    UserLogin = new UserLoginEntity()
                //    {
                //        UserName = "komal111",
                //        Password = "12320",
                //    },

                //    UserCommunication = new UserCommunicationEntity()
                //    {
                //        MobileNo = "9854345679",
                //        EmailId = "komal@yahoo.com"
                //    }
                //});

                //#endregion

                #region Delete

                Boolean flag = await userObj.DeleteAsync(new UserEntity()
                {
                    UserId = 3
                });

                #endregion

            }).Wait();
        }
    }
}
