﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.User.Interface
{
    public interface IUserCommunicationEntity
    {
        int UserId { get; set; }

        String MobileNo { get; set; }

        String EmailId { get; set; }
    }
}
