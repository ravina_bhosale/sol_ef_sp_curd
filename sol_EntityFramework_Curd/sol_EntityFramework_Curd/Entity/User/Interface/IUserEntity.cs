﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.User.Interface
{
    public interface IUserEntity
    {
         int UserId { get; set; }

         String FirstName { get; set; } 

         String LastName { get; set; }

        UserCommunicationEntity UserCommunication { get; set; }

        UserLoginEntity UserLogin { get; set; }
    }
}
