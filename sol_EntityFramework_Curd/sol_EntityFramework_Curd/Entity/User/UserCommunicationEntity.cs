﻿using sol_EntityFramework_Curd.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.User
{
    public class UserCommunicationEntity : IUserCommunicationEntity
    {
        public int UserId { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}
