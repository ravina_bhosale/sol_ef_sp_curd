﻿using sol_EntityFramework_Curd.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_EntityFramework_Curd.User
{
    public class UserLoginEntity : IUserLoginEntity
    {
        public int UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
